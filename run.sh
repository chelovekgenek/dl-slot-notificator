#!/bin/sh

export PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/home/pi/.nvm/versions/node/v14.17.2/bin

yarn build
yarn start
