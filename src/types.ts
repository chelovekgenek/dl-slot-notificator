interface ParseItem {
  available: boolean
  date: string
  selectable: boolean
}
