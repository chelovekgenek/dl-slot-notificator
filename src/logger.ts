import chalk from "chalk";

export class Logger {
  static log(message: string): void {
    console.log(`${chalk.blue(new Date().toISOString())}: ${message}`)
  }
}
