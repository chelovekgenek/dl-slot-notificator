import * as dotenv from "dotenv"

dotenv.config()

export default {
  isProduction: String(process.env.NODE_ENV) === "production",
  telegram: {
    apiKey: String(process.env.TELEGRAM_BOT_KEY),
    chatId: "334607509"
  },
  registru: {
    url: "https://programari2.registru.md/",
    values: {
      idnp: "2005042078239",
      serviceId: "867",
      officeId: "36",
      docId: "0372642",
    }
  }
}
