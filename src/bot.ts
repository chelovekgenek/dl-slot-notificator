import chalk from "chalk"
import TelegramBot from "node-telegram-bot-api"

import config from "./config"
import { Logger } from "./logger"

let lastCheckedAt: Date
let items: ParseItem[] = []

export const instance = new TelegramBot(config.telegram.apiKey, { polling: true })

export const broadcast = (message: string): void => { instance.sendMessage(config.telegram.chatId, message) }
export const update = (newItems: ParseItem[] = []): void => {
  lastCheckedAt = new Date()
  items = newItems
}

instance.onText(/\/status/, (msg) => {
  if (!lastCheckedAt) {
    instance.sendMessage(msg.chat.id, `Didn't run the script since last app start`);
  } else {
    instance.sendMessage(msg.chat.id, `Last check was at ${lastCheckedAt.toISOString()}`);

    if (!items.length) return
    instance.sendMessage(msg.chat.id, `There are available appointments at ${items.map(item => item.date).join(", ")};`);
  }
});

instance.onText(/\/slots/, (msg) => {
  if (!lastCheckedAt) {
    instance.sendMessage(msg.chat.id, `Didn't run the script since last app start`);
  } else {
    instance.sendMessage(msg.chat.id, `Last check was at ${lastCheckedAt.toISOString()}`);
  }
});

instance.on('message', (msg) => {
  Logger.log(`${chalk.green(msg.chat.username)}(${chalk.yellow(msg.chat.id)}) - ${msg.text}`)
});
