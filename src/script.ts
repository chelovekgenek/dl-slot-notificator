import puppeteer from "puppeteer"
import { isEqual } from "lodash"

import { delay } from "./delay"
import config from "./config"

export const runScript = async (page: puppeteer.Page): Promise<ParseItem[]> => {
  await page.select("#serviceId", config.registru.values.serviceId)

  const elements = await page.$$("#terms > p > input")
  for (const element of elements) {
    await element.press("Space")
  }

  await page.evaluate(({ idnp }) => {
    const element = document.querySelector("#idnp") as HTMLInputElement
    element.value = idnp
  }, config.registru.values)

  await page.evaluate(({ docId }) => {
    const element = document.querySelector("#schoolDocNr") as HTMLInputElement
    element.value = docId
  }, config.registru.values)

  await page.select("#officeId", config.registru.values.officeId)

  page.click("#btnCheck")
  await delay(5000)

  const classes = await page.$$eval("#datepicker", (el) => el.map((x) => x.getAttribute("class")))
  if (!isEqual(classes, ["datepicker hasDatepicker"])) return []

  const cells = await page.$$eval<ParseItem[]>(".picker-cell", (el) =>
    el.map((x) => {
      const classes = x.getAttribute("class").trim().split(" ")

      return {
        available: x.getAttribute("title") === "Disponibil",
        date: classes.find((item) => item.startsWith("picker-cell-"))?.replace("picker-cell-", ""),
        selectable: !classes.some((item) => item === "ui-state-disabled"),
      }
    }),
  )
  return cells.filter((cell) => cell.available)
}
