import puppeteer from "puppeteer"
import chalk from "chalk"
import cron from "node-cron"

import * as bot from "./bot"
import { runScript } from "./script"
import { Logger } from "./logger"
import config from "./config"

let running = false

const run = async () => {
  if (running) return
  running = true
  try {
    const browser = await puppeteer.launch({
      headless: true,
      slowMo: 1000,
      args: ["--no-sandbox"],
      executablePath: config.isProduction ? "/usr/bin/chromium-browser" : undefined
    })
    const page = await browser.newPage()
    page.setDefaultNavigationTimeout(0)
    await page.goto(config.registru.url, {
      waitUntil: "load",
      timeout: 0,
    })

    const slots = await runScript(page)
    bot.update(slots)
    if (!slots.length) {
      Logger.log(`There are ${chalk.red("no")} available slots;`)
    } else {
      const values = slots.map((slot) => `${slot.date} - ${String(slot.selectable)}`)
      Logger.log(`There are available appointments at ${chalk.green(values.join(", "))};`)

      const selectableValues = slots.filter(slot => slot.selectable).map(slot => slot.date)
      if (selectableValues.length)
        bot.broadcast(`There are available appointments at ${selectableValues.join(", ")}`)
    }

    await page.close()
    await browser.close()
  } catch (e) {
    Logger.log(e)
    bot.broadcast(`Error - ${e}`)
  } finally {
    running = false
  }
}

// every 3 minutes from 00:00 to 05:59
cron.schedule("*/3 0-5 * * 1-5", run)

// every minute from 06:00 to 08:59
cron.schedule("*/1 6-8 * * 1-5", run)

// every 5 minutes from 09:00 to 23:59
cron.schedule("*/5 9-23 * * 1-5", run)
