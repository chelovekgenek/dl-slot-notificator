FROM node:12.18.3

WORKDIR /usr/src/app

COPY . .

RUN apt-get update && \
  apt-get install -y libgtk2.0-0 libgtk-3-0 libnotify-dev \
  libgconf-2-4 libnss3 libxss1 \
  libasound2 libxtst6 xauth xvfb \
  libgbm-dev
RUN yarn install --production=true && \
  yarn build;

CMD [ "yarn", "start" ]
